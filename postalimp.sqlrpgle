**FREE

///
// Postalcodes Boundaries Importer
//
// This program extracts the postalcode boundary coordinates from the XML file
// and writes them to the database.
//
// All coordinates for one postalcode are saved in one record. This can be a
// very long record, like 80.000 characters. To be able to save that many
// characters in one field SQL is used. The field is defined as CLOB. The RPG
// variable is declaraed as SQLTYPE(CLOB : 100000).
//
// If an entry cannot be written to the database it will be logged in a spooled
// file.
//
// \author Mihael Schmidt
// \date 2017-10-06
//
// \link https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_73/rzajp/rzajpirpglobhost.htm LOB host variables in ILE RPG applications that use SQL
///


ctl-opt dftactgrp(*no) actgrp(*caller) main(main);


dcl-c LOG_LINE_LENGTH 132;

dcl-f QPRINT printer(LOG_LINE_LENGTH) usropn;

dcl-pr sendMessage extpgm('QMHSNDPM');
  msgID char(7) const;
  qlfMsgF char(20) const;
  msgData char(256) const;
  msgDataLen int(10) const;
  msgType char(10) const;
  callStkEnt char(10) const;
  callStkCnt int(10) const;
  msgKey char(4);
  error char(1024) options(*varsize) noopt;
end-pr;

dcl-pr executeCommand extpgm('QCMDEXC');
  command  char(32702) const options(*varsize);
  length packed(15:5) const;
  dbcsFlag char(3) const options(*nopass);
end-pr;

dcl-pr memcpy pointer extproc('memcpy');
  dest pointer value;
  source pointer value;
  count uns(10) value;
end-pr;

dcl-ds qusec qualified template;
  bytesProvided int(10);
  bytesAvailable int(10);
  exceptionId char(7);
  reserved char(1);
end-ds;
      
dcl-ds xmldata_t  qualified template;
  lastAttribute char(100);
  lastElement char(100);
  postalcode char(5);
  city char(100);
  numberPostalcodes int(10);
  numberErrors int(10);
end-ds;


///
// PEP
///
dcl-proc main;
  dcl-s command char(100);
  dcl-s file char(1024) inz('postalcodes.kml');
  dcl-ds xmlCommArea likeds(xmldata_t) inz;
    
  command = 'CLRPFM POSTALCODE';
  executeCommand(command : %len(%trim(command)));

  xml-sax %handler(xmlHandler : xmlCommArea) %xml(%trimr(file) : 'doc=file');

  if (xmlCommArea.numberErrors > 0);
    sendStatusMessage(%char(xmlCommArea.numberErrors) + 
      ' errors during import. See log.');
  else;
    sendStatusMessage('Sucessfully imported ' + 
      %char(xmlCommArea.numberPostalcodes) + ' postalcodes.');
  endif;

  // close log if anything has been written to it
  if (%open(QPRINT));
    close QPRINT;
  endif;
end-proc;


///
// \brief XML handler procedure
//
// Extract the coordinates from the XML data.
// 
///
dcl-proc xmlHandler;
  dcl-pi *N int(10);
    commArea likeds(xmldata_t);
    event int(10) value;
    string pointer value;
    stringLen int(20) value;
    exceptionId int(10) value;
  end-pi;

  dcl-s returnValue int(10);
  dcl-s value char(100) based(string);

  select;
    when (event = *XML_START_ELEMENT);
      commArea.lastElement = %subst(value : 1 : stringLen);

    when (event = *XML_CHARS);
      if (commArea.lastElement = 'name');
        commArea.postalcode = %trim(%subst(value : 1 : stringLen));
      elseif (commArea.lastElement = 'description');
        if (%subst(value : 1 : stringLen) <> *blank);
          commArea.city = %trim(%subst(value : 1 : stringLen));

          // remove postalcode from name : 52538 Gangelt, Selfkant
          if (%subst(commArea.city : 1 : 5) = commArea.postalcode);
            commArea.city = %trim(%subst(commArea.city : 6));
          endif;
        endif;
      elseif (commArea.lastElement = 'coordinates');
        writeCoords(commArea.postalcode : commArea.city : string : stringLen : 
                    commArea.numberPostalcodes : commArea.numberErrors);
      endif;

    when (event = *XML_END_ELEMENT);
      commArea.lastElement = *blank;
      commArea.lastAttribute = *blank;

      if (%subst(value : 1 : stringLen) = 'Placemark');
        clear commArea;
      endif;
  endsl;

  return returnValue;
end-proc;

///
// Writing coordinates to database
///
dcl-proc writeCoords;
  dcl-pi *N;
    postalcode char(5) const;
    city char(100) const;
    value pointer const;
    stringLength int(20);
    numberPostalcodes int(10);
    numberErrors int(10);
  end-pi;

  dcl-s coords sqltype(clob : 100000);

  clear coords;
  coords_len = stringLength;
  memcpy(%addr(coords_data) : value : stringLength);
  EXEC SQL INSERT INTO postalcodes(country, postalcode, city, coords)
           VALUES('DE', :postalcode, :city, :coords);
  if (sqlcod = 0);
    // ok
    numberPostalcodes += 1;
  elseif (sqlcod = -803);
    log('There is already an entry for the postalcode ' + postalcode +
        '. Skipping this one.');
    numberErrors += 1;
  else;
    log('An error occured at adding the postalcode ' + postalcode +
        ' to the database. SQL: ' + %char(sqlcod));
    numberErrors += 1;
  endif;
end-proc;

dcl-proc log;
  dcl-pi *N;
    message char(LOG_LINE_LENGTH) const;
  end-pi;

  dcl-ds logline len(LOG_LINE_LENGTH) end-ds;

  if (not %open(QPRINT));
    open QPRINT;
    
    // print header
    clear logline;
    write QPRINT logline;
    logline = 'Importing German Postalcodes';
    write QPRINT logline;
    logline = '============================';
    write QPRINT logline;
    clear logline;
    write QPRINT logline;
  endif;

  logline = message;
  write QPRINT logline;
end-proc;

dcl-proc sendStatusMessage;
  dcl-pi *N;
    message char(256) const;
  end-pi;
  
  dcl-s messageKey char(4);
  dcl-ds errorCode likeds(qusec);
  
  clear errorCode;
  clear messageKey;
  sendMessage(
         'CPF9898' :
         'QCPFMSG   *LIBL' :
         %trimr(message) :
         %len(%trimr(message)) :
         '*STATUS' :
         '*EXT' :
         *zero :
         messageKey :
         errorCode);
end-proc;