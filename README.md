# Postalcodes Boundaries Import

This is an example project which covers multiple topics:

- input XML processing using XML-SAX opcode
- handling character fields larger than 32k in SQL (using SQLTYPE CLOB)
- simple spooled file output for not imported data
- output result to using via message to status line

## Requirements

This example project does not require anything besides the RPG compiler.

## Installation

The file `postalcodes.sql` contains the SQL statement which builds the SQL table
for the weather data. It can be executed with the command `RUNSQLSTM` and creates
the table and index in the *current library*.

`postalimp.sqlrpgle` contains all the source code needed to import the postalcode
boundaries. This program does not need commitment control so it can be disabled
on compile with the parameters `COMMIT(*NONE)`.

The postalcode boundaries for the postalcode areas in Germany can be fetched from
suche-postleitzahl.org, https://www.suche-postleitzahl.org/download_files/public/plz-gebiete.kmz .
The *kmz* file is a zipped KML file. It can be extracted with

    java -jar plz-gebiete.kmz

Then rename the file to `postalcodes.kml`.

Note: The XML file from suche-postleitzahl.org does contain special characters
      like accented characters and German Umlaute. So this file should have a
      CCSID on your system which supports these, f. e. UTF-8 = CCSID 1208.
      You can check this by starting QSH, move to the directory with the 
      postalcodes stream file and use `ls -S` to display the CCSIDs of the files.

## Usage

Just `CALL POSTALIMP` will start the program. The current directory should be
set to the directory which contains the `postalcodes.kml` file as the program
expects it in the current directory.

If any errors occuring during the execution they will be logged in the created
spooled file. If it runs completely without any errors then no spooled file
will be created.