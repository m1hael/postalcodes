CREATE OR REPLACE TABLE postalcodes FOR SYSTEM NAME postalcode (
  id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
  postalcode CHAR(5) NOT NULL,
  country CHAR(2),
  city CHAR(100),
  coordinates FOR coords CLOB(100000) NOT NULL
) RCDFMT postalcof1;

-- DROP INDEX postalcodes_postalcode_country;
CREATE UNIQUE INDEX postalcodes_postalcode_country FOR SYSTEM NAME postalidx1
  ON postalcodes (country, postalcode);